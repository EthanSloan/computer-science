QUEEN�S UNIVERSITY OF BELFAST COMPUTER SCIENCE

Minute of Project Supervision Meeting
======================================

Student Name:
Ethan SloanProject Module Code:
BD05
Project Supervisor:
Barry DevereuxMeeting Number:4
Date of Meeting:Thursday 5th November 2020

Progress since last meeting, and decisions arrived at during meeting:
Began working with Google Colab and Language Models
Discussed progress needed for interim demo


Action Points:
Continue with Google Colab, working with language models and MRI/EEG datasets


Date of next meeting:
Thursday 12th November 2020




Agreed minute should be initialled by the supervisor.

Supervisor's Initials:	_____________________________________________	Date: _________________



Supervisor's Comments:
=======================

