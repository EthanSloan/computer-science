2020_CSC3002_Ethan_Neuro

# CSC3002 BD05 Alice in Neuroland: building models of language processing in the human cortex

## Ethan Sloan 40207000

Text of the original project proposal:

There are interesting correspondences between how the human brain is believed to process speech input and how computational language models are trained. Prominent neurocognitive theories of brain function suggest that the brain isa predictive machine --it has a model of the world, which actively makes predictions about upcoming sensory input, and uses the mismatch between predicted input and the actual input (i.e. its prediction error) to update its model of the world (Clark, 2013; Friston, 2010). In the context of language comprehension, this means that the brain uses linguistic knowledge to constrain or make predictions about the probabilities of upcoming words in a sentence, conditioned on the preceding input (Kuperberg & Jaeger, 2016). The task of language modelling adopts a similar strategy –we train a model that can learn to represent the next word in a sequence, given the history of previous words (Karpathy et al 2015).
However, although computational language modelling and theories of human language understanding share this common perspective, it is unclear whether there is any correspondence between the kinds of representations that computational language models learn and the functional organisation of language processing in the brain (Jain & Huth, pp2018).
In this project, we will use pretrained language models to investigate and model human language understanding. We will investigate expectation/surprisal in the trained model, as well investigate how the model represents linguistic information internally. We will investigate the relationship (if any) between the trained language model and the brain, by relating the representations in the trained model to fMRI and EEG neuroimaging data collected as people listened to the audiobook of Alice in Wonderland (Bhattasali et al 2020).
In this project, you will:
*  Become familiar with neural network language modelling and implementations of language modelling in Pytorch
*  Become familiar with the neuroimaging data and standard statistical techniques for the analysis of such data (voxel receptive field modelling; representational similarity analysis)
*  Finetuning language models
*  Investigate the representation of linguistic knowledge in the trained model
*  Statistically test the representations and output of the language model against the brain activity data

This project would be suitable for a student with Python programming skills.